<?php
        //金字塔的初始行数
        $line = 1;
        while($line <= 5){
            //组成金字塔空格和星星的初始位置
            $empty_pos = $star_pos = 1;
            //金字塔每行输出的最多空格数
            $empty = 5 - $line;
            //组成金字塔每行的最多星星数
            $star = 2*$line-1;
            //输出金字塔每行的空格
            while($empty_pos <= $empty){
                echo '&nbsp;';
                ++$empty_pos;
            }
            //输出金字塔每行的星星
            while($star_pos <= $star){
                echo '*';
                ++$star_pos;
            }
            echo '<br>';
            $line++;
        }
        ?>
 